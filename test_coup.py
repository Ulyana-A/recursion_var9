from main import coup


def test_coup_good():
    assert coup(654) == 456

def test_coup_bad():
    assert coup(654) != 654
