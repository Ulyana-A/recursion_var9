def coup(n, r=0):
    if n > 0:
        r = r * 10 + n % 10
        return coup(n // 10, r)
    if n == 0:
        return r

if __name__ == "__main__":
    n = int(input("введите число: "))
    print(coup(n))
